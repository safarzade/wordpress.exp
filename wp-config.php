<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'wordpress.exp' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', '' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         '=$dr)_?H/qBk7?_^n7l)1nT)?+P_x_WF:>7$BrpFhTR$%a/!a&*DXl AHw$S&`i{' );
define( 'SECURE_AUTH_KEY',  'yM9!v9F;n/hvwE&hH>!4Pwrt @MuM{9Imk%sv:xGZjD%*(B=X6GS^.y2fDZpFkQI' );
define( 'LOGGED_IN_KEY',    'WG/~l]({B.`TdilP?}+q/BW^A%`N% Fh61g-V4kzik(>hKJ$PEB@_XzuF0`_L*lE' );
define( 'NONCE_KEY',        'ah[5a8$#E4C`pAlwa5{9=@N2JsJ#5=A_+~xjS8aEd4_/8~&WhMLxzuKq2Z-a}CLL' );
define( 'AUTH_SALT',        'S&%5A^d{TAK*YO# [C`Yp+!*XR_uGC,]9v`KP9.TzzMg>Zb}$v.h4~7E>3p4;5g%' );
define( 'SECURE_AUTH_SALT', '_E(Gp_Z+OjZMf&;Myu=fwy(-4dIn8uh5$F[%Kf:!w(WzD6NcGQ/KVY#oZPzyc!/_' );
define( 'LOGGED_IN_SALT',   'M$vfrGGLK6_`qc1w:fPpPH`{j#h!*NF[OFbrI{_!R{PpUQ02/HAL:lQ1^StZ=~3(' );
define( 'NONCE_SALT',       '4ZN#H,}*c~(4:XU:2o jr)[{%9jP%wpotHO|pNZj<&koC Y~:(mUxD+B#j>|zI+q' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'exp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
