<?php
/*
Plugin Name: Hooks Plugin
Plugin URI: https://google.com/
Description: This Is The Hooks Plugin
Author: Mostafa Akrami
Author URI: https://google.com/
Text Domain: myFirstPlugin
Domain Path: /languages/
Version: 1.0.0
*/

/////////////////////////////////////////////////////////////////////////////////////////

// function run1(){
//     print "salam<br>";
// }
// function run2()
// {
//     print "Khobi?<br>";
// }
// function run3()
// {
//     print "bye";
// }
// add_action("order_purchased" , "run1");
// add_action("order_purchased" , "run2");
// add_action("order_purchased" , "run3");
// do_action("order_purchased");

/////////////////////////////////////////////////////////////////////////////////////////

// function price1($value){
//     return $value * 2;
// }
// function price2($value){
//     return $value - 100;
// }
// function price3($value){
//     return $value * 100;
// }
// add_filter("get_price" , "price1");
// add_filter("get_price" , "price2");
// add_filter("get_price" , "price3");
// $result = apply_filters("get_price" , 25000);
// print $result;

//////////////////////////////////////////////////////////////////////////////////////////

