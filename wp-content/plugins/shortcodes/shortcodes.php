<?php
/*
Plugin Name: shortcodes
Plugin URI: https://google.com/
Description: shortcodes
Author: Mostafa Akrami
Author URI: https://google.com/
Text Domain: shortcodes
Domain Path: /languages/
Version: 1.0.0
*/

////////////////////////////////////////////////////////////////////////////////////

// function login_shortcode($atts){
//     // var_dump($atts);
//     return "<h1>SALAM " . $atts['role'] . "</h1>";
// }
// add_shortcode("login" , "login_shortcode");

/////////////////////////////////////////////////////////////////////////////////////

function login_page_shortcode(){
    return "
        <form>
            <label for='username'>user name:</label><br>
            <input type='text'><br>
            <label for='password'>password:</label><br>
            <input type='password'><br><br>
            <input type='submit' value='Submit'>
        </form>
    ";
}

add_shortcode("login_page" , "login_page_shortcode");