jQuery(document).ready(function ($) {
    $('#loginForm').on('submit' , function(e){
        e.preventDefault();
        // alert(123);
        let userEmail = $('#userEmail').val();
        let userPassword = $('#userPassword').val();
        let notify = $('.alert');
        $.ajax({
            type: "post",
            url: "/wp-admin/admin-ajax.php",
            data: {
                action: "wp_auth_login",
                user_email: userEmail,
                user_password: userPassword
            },
            dataType: "json",
            success: function (response) {
                if(response.success){
                    notify.removeClass('alert-error').addClass("alert-success");
                    notify.slideDown();
                    notify.html(response.message);
                    setTimeout(function(){
                        window.location.href = '/';
                    },1200);
                }
                
            },
            error: function (error) {
                // console.log(error.responseJSON.message);
                if(error){
                    let message = error.responseJSON.message;
                    notify.addClass("alert-error");
                    notify.slideDown();
                    notify.html(message);
                    // if (message == "ایمیل نمی تواند خالی باشد"){
                    //     $('#userEmail').css('border', '1px solid  rgb(255, 55, 55)');
                        
                    // }
                    // else if (message == "پسوورد نمی تواند خالی باشد"){
                    //     $('#userPassword').css('border', '1px solid  rgb(255, 55, 55)');
                    //     return false;
                    // }
                    // else if (message == "ایمیل وارد شده معتبر نمی باشد . "){
                    //     $('#userEmail').css('border', '1px solid  rgb(255, 55, 55)');
                    //     return false;
                    // }
                    return false;
                    // notify.css('display' , 'block');
                    // notify.delay(3000).hide(300);
                    // $('#span1').on('click' , function(){
                    //     notify.fadeOut();
                    // });
                }
            }
        });
    });

    $('#registerForm').on('submit', function (e) {
        e.preventDefault();
        // alert(321);
        let user_name = $('#user_name').val();
        let user_lastname = $('#user_lastname').val();
        let user_email = $('#user_email').val();
        let user_password = $('#user_password').val();
        let notify = $('.alert');
        $.ajax({
            type: "post",
            url: "/wp-admin/admin-ajax.php",
            data: {
                action: "wp_auth_register",
                user_name: user_name,
                user_lastname: user_lastname,
                user_email: user_email,
                user_password: user_password
            },
            dataType: "json",
            success: function (response) {
                if (response.success) {
                    notify.removeClass('alert-error').addClass("alert-success");
                    notify.slideDown();
                    notify.html(response.message);
                    setTimeout(function () {
                        window.location.href = '/ورود';
                    }, 1200);
                }
            },
            error: function (error) {
                if (error) {
                    let message = error.responseJSON.message;
                    notify.addClass("alert-error");
                    notify.slideDown();
                    notify.html(message);
                }
            }
        });
    });
});