<?php

function wp_auth_do_login()
{
    // var_dump($_POST);
    $user_email =  sanitize_text_field($_POST['user_email']);
    $user_password =  sanitize_text_field($_POST['user_password']);
    $validation_result = wp_auth_validate_email_and_password($user_email, $user_password);

    // var_dump($user_email , $user_password);
    // var_dump($validation_result);

    if (!$validation_result['is_valid']) {
        wp_send_json([
            'success' => false,
            'message' => $validation_result['message']
        ], 403);
    }

    $user = wp_authenticate_email_password(null , $user_email , $user_password);
    if(is_wp_error($user)){
        wp_send_json([
            'success' => false,
            'message' => 'کاربری با این مشخصات یافت نشد . '
        ], 403);
    }

    $loginResult = wp_signon([
        'user_login'    => $user->user_login,
        'user_password' => $user_password,
        'remember'      => false
    ]);
    if (is_wp_error($loginResult)) {
        wp_send_json([
            'success' => false,
            'message' => 'در حال حاضر امکان ورود به سایت نمی باشد . لطفا بعدا امتحان کنید '
        ], 403);
    }

    wp_send_json([
        'success' => true,
        'message' => 'عملیات ورود با موفقیت انجام شد'
    ], 200);
}

function wp_auth_validate_email_and_password($email, $password)
{

    $result = [
        'is_valid' => true,
        'message' => ''
    ];

    if (empty($email)) {
        $result['is_valid'] = false;
        $result['message'] = "ایمیل نمی تواند خالی باشد";
        return $result;
    }

    if (empty($password)) {
        $result['is_valid'] = false;
        $result['message'] = "پسوورد نمی تواند خالی باشد";
        return $result;
    }

    if (!is_email($email)) {
        $result['is_valid'] = false;
        $result['message'] = "ایمیل وارد شده معتبر نمی باشد . ";
        return $result;
    }

    return $result;
}

function wp_auth_do_register(){
    // var_dump($_POST);
    $user_name =  sanitize_text_field($_POST['user_name']);
    $user_lastname =  sanitize_text_field($_POST['user_lastname']);
    $user_email =  sanitize_text_field($_POST['user_email']);
    $user_password =  sanitize_text_field($_POST['user_password']);
    $validation_result = validate_register_request($user_name, $user_lastname, $user_email, $user_password);

    // var_dump($user_name, $user_lastname, $user_email, $user_password);

    if (!$validation_result['is_valid']) {
        wp_send_json([
            'success' => false,
            'message' => $validation_result['message']
        ], 422);
    }

    $userEmailPart = explode("@" , $user_email);
    $newUser = wp_insert_user([
        'user_login' => apply_filters("pre_user_login" , $userEmailPart[0] . rand(1000 , 9999)),
        'user_pass' => apply_filters("pre_user_pass" , $user_password),
        'user_email' => apply_filters("pre_user_email" , $user_email),
        'first_name' => apply_filters("	pre_user_first_name" , $user_name),
        'last_name' => apply_filters("pre_user_last_name" , $user_lastname),
        'display_name' => apply_filters("pre_user_display_name" , "{$user_name} {$user_lastname}")
    ]);

    if (is_wp_error($newUser)) {
        wp_send_json([
            'success' => false,
            'message' => 'خطایی در حین ثبت نام شما رخ داده است . لطفا بعدا امتحان کنید'
        ], 500);
    }
    wp_send_json([
        'success' => true,
        'message' => 'ثبت نام شما با موفقیت انجام شد . '
    ], 200);
}

function validate_register_request($name , $lastname , $email , $password){
    $result = [
        'is_valid' => true,
        'message' => ''
    ];

    if(empty($name) || empty($lastname) || empty($email) || empty($password)){
        $result['is_valid'] = false;
        $result['message'] = 'پر کردن فیلدها الزامی می باشد . ';
        return $result;
    }

    if (!is_email($email)) {
        $result['is_valid'] = false;
        $result['message'] = "ایمیل وارد شده معتبر نمی باشد . ";
        return $result;
    }

    if(email_exists($email)){
        $result['is_valid'] = false;
        $result['message'] = 'این ایمیل توسط شخص دیگری مورد استفاده می باشد . ';
        return $result;
    }

    return $result;
}

add_action("wp_ajax_nopriv_wp_auth_login", "wp_auth_do_login");
add_action("wp_ajax_nopriv_wp_auth_register", "wp_auth_do_register");
