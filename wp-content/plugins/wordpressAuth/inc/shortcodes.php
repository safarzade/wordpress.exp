<?php

function wp_auth_login_handler($atts , $content = null){
    $wp_auth_options = get_option("wp_auth_options" , []);
    if(isset($wp_auth_options["is_login_active"]) && !$wp_auth_options["is_login_active"]){
        return "<div><p>در حال حاضر امکان ورود به سایت نمی باشد . لطفا بعدا تلاش نمایید .</p></div>";
    }
    include TPL_AUTH . "/front/login.php";
}

function wp_auth_register_handler($atts, $content = null){
    $wp_auth_options = get_option("wp_auth_options", []);
    if (isset($wp_auth_options["is_register_active"]) && !$wp_auth_options["is_register_active"]) {
        return "<div><p>در حال حاضر امکان ثبت نام در سایت نمی باشد . لطفا بعدا تلاش نمایید .</p></div>";
    }
    include TPL_AUTH . "/front/register.php";
}

add_shortcode("wp_auth_login" , "wp_auth_login_handler");
add_shortcode("wp_auth_register", "wp_auth_register_handler");

