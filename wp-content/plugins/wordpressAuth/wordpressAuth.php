<?php
/*
Plugin Name: wordpressAuth
Plugin URI: https://google.com/
Description: wordpress plugin to manage auth
Author: Mostafa Akrami
Author URI: https://google.com/
Text Domain: Wordpress Auth
Domain Path: /languages/
Version: 1.0.0
*/

define("DIR_AUTH" , plugin_dir_path(__FILE__));
define("URL_AUTH" , plugin_dir_url(__FILE__));
define("INC_AUTH" , DIR_AUTH . "/inc");
define("TPL_AUTH", DIR_AUTH . "/tpl");

include INC_AUTH . "/functions.php";
include INC_AUTH . "/shortcodes.php";
include INC_AUTH . "/ajax.php";
if(is_admin()){
    include INC_AUTH . "/admin.php";
}