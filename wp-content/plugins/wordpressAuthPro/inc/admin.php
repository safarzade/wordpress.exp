<?php

function wp_authpro_admin_setting()
{
    add_menu_page(
        'مدیریت ثبت نام',
        'مدیریت ثبت نام',
        'manage_options',
        'wp_auth_pro',
        'wp_authpro_settings'
    );
}

function wp_authpro_settings()
{
    $wp_authpro_options = get_option('wp_authpro_options' , []);
    // var_dump($wp_authpro_options);
    if(isset($_POST["saveData"])){
        // var_dump($_POST);
        $wp_authpro_options["is_login_active"] = isset($_POST["is_login_active"]);
        $wp_authpro_options["is_register_active"] = isset($_POST["is_register_active"]);
        $wp_authpro_options["login_form_title"] = sanitize_text_field($_POST["login_form_title"]);
        $wp_authpro_options["register_form_title"] = sanitize_text_field($_POST["register_form_title"]);

        update_option("wp_authpro_options" , $wp_authpro_options);
    }

    function wporg_myplugin_add_registration_fields(){
        
    }

    add_action('register_form', 'wporg_myplugin_add_registration_fields' );

    include TPL_AUTH_PRO . "/admin/settings.php";
}

add_action("admin_menu", "wp_authpro_admin_setting");