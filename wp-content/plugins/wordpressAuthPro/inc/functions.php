<?php

function wp_authpro_load_assets()
{
    wp_register_style("wp_authpro_style", URL_AUTH_PRO . "/assets/css/auth.css");
    wp_enqueue_style("wp_authpro_style");

    wp_register_script("wp_authpro_script", URL_AUTH_PRO . "/assets/js/auth.js", ['jquery'], "", true);
    wp_enqueue_script("wp_authpro_script");
}

add_action("admin_enqueue_scripts", "wp_authpro_load_assets");
add_action("wp_enqueue_scripts", "wp_authpro_load_assets");

