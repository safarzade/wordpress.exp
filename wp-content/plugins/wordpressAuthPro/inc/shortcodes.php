<?php

function wp_authpro_login_handler($atts, $content = null)
{
    $wp_authpro_options = get_option("wp_authpro_options" , []);
    if(isset($wp_authpro_options["is_login_active"]) && !$wp_authpro_options["is_login_active"]){
        return "<div><p>در حال حاضر امکان ورود به سایت نمی باشد . لطفا بعدا تلاش نمایید .</p></div>";
    }
    include TPL_AUTH_PRO . "/front/login.php";
}

function wp_authpro_register_handler($atts, $content = null)
{
    $wp_authpro_options = get_option("wp_authpro_options" , []);
    if (isset($wp_authpro_options["is_register_active"]) && !$wp_authpro_options["is_register_active"]) {
        return "<div><p>در حال حاضر امکان ثبت نام در سایت نمی باشد . لطفا بعدا تلاش نمایید .</p></div>";
    }
    include TPL_AUTH_PRO . "/front/register.php";
}

add_shortcode("wp_authpro_login" , "wp_authpro_login_handler");
add_shortcode("wp_authpro_register", "wp_authpro_register_handler");