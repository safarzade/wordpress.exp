<div class="wrap">
    <h1>تنظیمات</h1>
    <div id="nav-tab">
        <button id="default" class="tab-btn active" onclick="select(event , 'general')">عمومی</button>
        <button class="tab-btn active" onclick="select(event , 'register')">ثبت نام</button>
        <button class="tab-btn active" onclick="select(event , 'login')">ورود</button>

        <div class="content">
            <div id="general" class="tab-content">
                <form action="" method="post">
                    <table class="form-table">
                        <tr valign="top">
                            <th scope="row">فعال بودن ورود</th>
                            <td>
                                <input type="checkbox" name="is_login_active" <?php echo $wp_authpro_options["is_login_active"] ? "checked" : ""; ?>>
                            </td>
                        </tr>
                        <tr valign="top">
                            <th scope="row">فعال بودن ثبت نام</th>
                            <td>
                                <input type="checkbox" name="is_register_active" <?php echo $wp_authpro_options["is_register_active"] ? "checked" : ""; ?>>
                            </td>
                        </tr>
                        <tr valign="top">
                            <th scope="row">عنوان فرم ورود</th>
                            <td>
                                <input type="text" name="login_form_title" value="<?php echo $wp_authpro_options["login_form_title"] ? $wp_authpro_options["login_form_title"] : ""; ?>">
                            </td>
                        </tr>
                        <tr valign="top">
                            <th scope="row">عنوان فرم ثبت نام</th>
                            <td>
                                <input type="text" name="register_form_title" value="<?php echo $wp_authpro_options["register_form_title"] ? $wp_authpro_options["register_form_title"] : ""; ?>">
                            </td>
                        </tr>
                        <tr valign="top">
                            <th scope="row"></th>
                            <td>
                                <input type="submit" name="saveData" value="ذخیره سازی" style="cursor: pointer;">
                            </td>
                        </tr>
                    </table>
                </form>
            </div>
            <div id="register" class="tab-content">
                <form action="" method="post">
                    <p style="font-size: 1.15rem;">مواردی را که مایل به نمایش در فرم ثبت نام هستید از بین گزینه های زیر انتخاب کنید : </p>
                    <table class="form-table">
                        <tr valign="top">
                            <th scope="row">نام</th>
                            <td>
                                <input type="checkbox" name="name">
                            </td>
                        </tr>
                        <tr valign="top">
                            <th scope="row">نام خانوادگی</th>
                            <td>
                                <input type="checkbox" name="family">
                            </td>
                        </tr>
                        <tr valign="top">
                            <th scope="row">نام کامل</th>
                            <td>
                                <input type="checkbox" name="fullName">
                            </td>
                        </tr>
                        <tr valign="top">
                            <th scope="row">تکرار کلمه عبور</th>
                            <td>
                                <input type="checkbox" name="rPassword">
                            </td>
                        </tr>
                        <tr valign="top">
                            <th scope="row">آدرس</th>
                            <td>
                                <input type="checkbox" name="address">
                            </td>
                        </tr>
                        <tr valign="top">
                            <th scope="row">کدپستی</th>
                            <td>
                                <input type="checkbox" name="postalCode">
                            </td>
                        </tr>
                        <tr valign="top">
                            <th scope="row">تلفن ثابت</th>
                            <td>
                                <input type="checkbox" name="tel">
                            </td>
                        </tr>
                        <tr valign="top">
                            <th scope="row" style="font-weight: lighter;">
                                <input type="submit" name="saveDataRegister" value="ذخیره سازی" style="cursor: pointer;">
                            </th>  
                        </tr>
                    </table>
                </form>
            </div>
            <div id="login" class="tab-content">
                <form action="" method="post">
                    <p style="font-size: 1.15rem;">نوع ورود کاربر را مشخص کنید : </p>
                    <table class="form-table">
                        <tr valign="top">
                            <th scope="row">ورود با ایمیل و کلمه عبور</th>
                            <td>
                                <input type="radio" name="loginType" checked>
                            </td>
                        </tr>
                        <tr valign="top">
                            <th scope="row">ورود با موبایل و کلمه عبور</th>
                            <td>
                                <input type="radio" name="loginType">
                            </td>
                        </tr>
                        <tr valign="top">
                            <th scope="row" style="font-weight: lighter;">
                                <input type="submit" name="saveDataLogin" value="ذخیره سازی" style="cursor: pointer;">
                            </th>  
                        </tr>
                    </table>
                </form>
            </div>
        </div>
    </div>
</div>