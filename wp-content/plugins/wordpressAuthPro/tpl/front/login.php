<div class="alert">
    <!-- <span id="span1">&#10540;</span> -->
</div>
<div class="auth-wrapper">
    <div class="login-wrapper">
        <?php if(isset($wp_authpro_options["login_form_title"])){ ?>
            <h2><?php echo $wp_authpro_options["login_form_title"] ?></h2>
        <?php } ?>
        <form action="" method="POST" id="loginForm">
            <div class="form-row">
                <label for="userEmail">ایمیل : </label>
                <input type="email" name="userEmail" id="userEmail">
            </div>
            <div class="form-row">
                <label for="userPassword">رمز ورود : </label>
                <input type="password" name="userPassword" id="userPassword">
            </div>
            <div class="form-row">
                <button name="submitLogin" class="button">ورود</button>
            </div>
        </form>
    </div>
</div>