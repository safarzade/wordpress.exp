<div class="alert"></div>
<div class="auth-wrapper">
    <div class="login-wrapper">
        <?php if (isset($wp_authpro_options["register_form_title"])) { ?>
            <h2><?php echo $wp_authpro_options["register_form_title"] ?></h2>
        <?php } ?>
        <form action="" method="POST" id="registerForm">
            <div class="form-row">
                <label for="user_name">نام : </label>
                <input type="text" name="user_name" id="user_name">
            </div>
            <div class="form-row">
                <label for="user_lastname">نام خانوادگی : </label>
                <input type="text" name="user_lastname" id="user_lastname">
            </div>
            <div class="form-row">
                <label for="user_email">ایمیل : </label>
                <input type="email" name="user_email" id="user_email">
            </div>
            <div class="form-row">
                <label for="user_password">رمز ورود : </label>
                <input type="password" name="user_password" id="user_password">
            </div>
            <div class="form-row">
                <button name="submitRegister" class="button">ثبت نام</button>
            </div>
        </form>
    </div>
</div>