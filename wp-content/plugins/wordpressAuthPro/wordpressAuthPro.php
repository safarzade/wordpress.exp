<?php
/*
Plugin Name: wordpressAuthPro
Plugin URI: https://google.com/
Description: wordpress plugin to manage auth pro
Author: Mostafa Akrami
Author URI: https://google.com/
Text Domain: Wordpress Auth pro
Domain Path: /languages/
Version: 1.0.0
*/

define("DIR_AUTH_PRO", plugin_dir_path(__FILE__));
define("URL_AUTH_PRO", plugin_dir_url(__FILE__));
define("INC_AUTH_PRO", DIR_AUTH_PRO . "/inc");
define("TPL_AUTH_PRO", DIR_AUTH_PRO . "/tpl");

include INC_AUTH_PRO . "/functions.php";
include INC_AUTH_PRO . "/shortcodes.php";

if (is_admin()) {
    include INC_AUTH_PRO . "/admin.php";
}