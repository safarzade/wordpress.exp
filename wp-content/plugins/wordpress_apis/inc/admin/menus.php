<?php

add_action("admin_menu" , "apis_register_menu");

function apis_register_menu(){
    // add_menu_page(
    //     'تنظیمات پلاگین',
    //     'تنظیمات پلاگین',
    //     'manage_options',
    //     'apis_admin',
    //     'wp_apis_main_menu_handler'
    // );

    // add_submenu_page(
    //     'apis_admin',
    //     'عمومی',
    //     'عمومی',
    //     'manage_option',
    //     'wp_apis_general',
    //     'wp_apis_general_page'
    // );

    // add_submenu_page(
    //     'apis_admin' ,
    //     'عمومی' ,
    //     'عمومی' ,
    //     'manage_option' ,
    //     'wp_apis_general' ,
    //     'wp_apis_general_page'

    // );

    add_menu_page(
        'پلاگین سفارشی', 
        'پلاگین سفارشی',
        'manage_options',
        'apis_admin',
        'wp_apis_main_menu_handler'
    );
    add_submenu_page(
        'apis_admin',
        'پلاگین سفارشی',
        'پلاگین سفارشی',
        'manage_options',
        'apis_admin',
    );
    add_submenu_page(
        'apis_admin',
        'تنظیمات',
        'تنظیمات',
        'manage_options',
        'my-secondary-slug',
        'wp_apis_general_page'
    );
    add_submenu_page(
        'apis_admin',
        'کاربران',
        'کاربران',
        'manage_options',
        'apis_user',
        'wp_apis_users_page'
    );
}


function wp_apis_main_menu_handler()
{
    global $wpdb;

    $action = $_GET["action"];
    // var_dump($action);
    if ($action == "delete") {
        $item = intval($_GET["item"]);
        if ($item > 0) {
            $wpdb->delete($wpdb->prefix."test2", ["ID" => $item]);
        } else {
            wp_die('درخواست نا معتبر', 'کاربری برای حذف انتخاب نشده است');
        }
    }
    if ($action == "update") {
        $item = intval($_GET["item"]);
        if ($item > 0 && isset($_POST["saveData"])) {
            
            $wpdb->update($wpdb->prefix . "test2",[
                "name" => $_POST["name"],
                "family" => $_POST["family"],
                "ip" => $_POST["ip"]
                ] , ["ID" => $item]
            );
        }
        $updates = $wpdb->get_results("SELECT * FROM {$wpdb->prefix}test2  WHERE ID = $item");
        include TPL . "/admin/menus/update.php";
    }
    if ($action == "add") {
        if (isset($_POST["saveData"])) {
            // var_dump($_POST);
            $wpdb->insert($wpdb->prefix . "test2", [
            "name" => $_POST["name"],
            "family" => $_POST["family"],
            "ip" => $_POST["ip"]
        ]);
        }
        include TPL . "/admin/menus/add.php";
    } 
    if($action != "add" && $action != "update") {
        $samples = $wpdb->get_results("SELECT * FROM {$wpdb->prefix}test2");

        // var_dump($samples);
        include TPL . "/admin/menus/main.php";
    }
}


function wp_apis_general_page()
{
    if (isset($_POST['saveSetting'])) {
        // $is_plugin_active = isset($_POST['myCheckBox']) ? '1' : '0';
        // var_dump($is_plugin_active);
        // add_option("wp_apis_is_active" , $is_plugin_active);
        if (isset($_POST['myCheckBox'])) {
            update_option("wp_apis_is_active", 1);
        } else {
            delete_option("wp_apis_is_active");
        }
        // update_option("wp_apis_is_active" , $is_plugin_active);
    }
    $current_plugin_status = get_option("wp_apis_is_active");

    include TPL . "/admin/menus/general.php";
}

function wp_apis_users_page(){

    $password = wp_generate_password( 10 );
    $userEmail = "mostafa.akrami@gmail.com";
    $userEmailDate = explode('@' , $userEmail);
    // wp_create_user($userEmailDate[0] , $password , $userEmail);

    // $newUser = wp_insert_user([
    //     'user_pass' => $password,
    //     'user_email' => $userEmail,
    //     'user_login' => $userEmailDate[0],
    //     'display_name' => 'کاربر جدید',
    // ]);

    // wp_update_user([
    //     "ID" => 6,
    //     "display_name" => "2222"
    // ]);

    // wp_delete_user(7 , 1);

    global $wpdb;
    $users = $wpdb->get_results("SELECT ID , user_email , display_name FROM {$wpdb->users}");

    if(isset($_GET["action"]) && $_GET["action"] == 'edit'){
        $userId = intval($_GET["id"]);
        if(isset($_POST["saveUserInfo"])){
            $mobile = $_POST["mobile"];
            $wallet = $_POST["wallet"];
            if(!empty($mobile)){
                update_user_meta($userId , 'mobile' , $mobile);
            }
            if (!empty($wallet) || $wallet == 0) {
                update_user_meta($userId, 'wallet', $wallet);
            }
        }
        $mobile = get_user_meta($userId , 'mobile' , true);
        $wallet = get_user_meta($userId, 'wallet', true);
        include TPL . "/admin/menus/users/edit.php";
        return;
    }

    if (isset($_GET["action"]) && $_GET["action"] == 'removeMobileAndWallet') {
        $userId = intval($_GET["id"]);
        delete_user_meta($userId , "mobile");
        delete_user_meta($userId , "wallet");
    }

    if(isset($_GET["action"]) && $_GET["action"] == 'removeMobile'){
        $userId = intval($_GET["id"]);
        delete_user_meta($userId, "mobile");
    }

    if (isset($_GET["action"]) && $_GET["action"] == 'removeWallet') {
        $userId = intval($_GET["id"]);
        delete_user_meta($userId, "wallet");
    }

    include TPL . "/admin/menus/users/users.php";
}