<?php

    add_action("wp_ajax_calculate_operation" , "wp_apis_ajax_calculate_operation");

    function wp_apis_ajax_calculate_operation(){
        $number1 = $_POST['numberOne'];
        $number2 = $_POST['numberTwo'];

        $userId = wp_get_current_user();

        wp_send_json([
            'success' => true,
            'result' => $number1 + $number2,
            'Id' => $userId->ID
        ]);
    }

?>