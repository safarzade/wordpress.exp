<div class="wrap">
    <h1>تنظیمات پلاگین</h1>

    <form action="" method="post">
        <label for="myCheckBox">
            <input name="myCheckBox" type="checkbox" id="myCheckBox" <?php echo isset($current_plugin_status) && intval($current_plugin_status) > 0 ? 'checked' : ''; ?>>
            فعال بودن پلاگین
        </label>
        <button class="button button-primary" type="submit" name="saveSetting">ذخیره سازی</button>
    </form>
</div>
