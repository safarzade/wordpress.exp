<div class="wrap">
    <h1>لیست اطلاعات</h1>
    <a style="text-decoration: none; font-size: 16px; color: #858585; background-color: #0ff; border-radius: 5px; padding: 3px;" href="<?php echo add_query_arg(["action" => "add"]) ?>">اضافه
        کردن کاربر جدید</a>
    <button class="button" id="sendAjaxRequest">sendAjaxRequest</button>
    <table class="widefat">
        <thead>
            <tr>
                <th>شناسه</th>
                <th>نام</th>
                <th>نام خانوادگی</th>
                <th>آی پی</th>
                <th>عملیات</th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($samples as $sample) { ?>
                <tr>
                    <td><?php echo $sample->ID; ?></td>
                    <td><?php echo $sample->name; ?></td>
                    <td><?php echo $sample->family; ?></td>
                    <td><?php echo $sample->ip; ?></td>
                    <td>
                        <a style="border: 1px solid #aaa; border-radius: 3px; padding: 3px; margin: 0 10px; margin-right: 0;" href="<?php echo add_query_arg(["action" => "delete", "item" => $sample->ID]) ?>">حذف</a>

                        <a style="border: 1px solid #aaa; border-radius: 3px; padding: 3px; margin: 0 10px;" href="<?php echo add_query_arg(["action" => "update", "item" => $sample->ID]) ?>">به روزرسانی</a>
                    </td>
                </tr>
            <?php } ?>
        </tbody>
    </table>



</div>