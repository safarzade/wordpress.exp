<div class="wrap">
    <h1>به روزرسانی کاربر</h1>
    <form method="POST" action="">
        <table class="form-table">
            <?php foreach ($updates as $update) { ?>
                <tr valign="top">
                    <th scope="row">نام</th>
                    <td>
                        <input type="text" name="name" value="<?php echo $update->name; ?>">
                    </td>
                </tr>
                <tr valign="top">
                    <th scope="row">نام خانوادگی</th>
                    <td>
                        <input type="text" name="family" value="<?php echo $update->family; ?>">
                    </td>
                </tr>
                <tr valign="top">
                    <th scope="row">آی پی</th>
                    <td>
                        <input type="text" name="ip" value="<?php echo $update->ip; ?>">
                    </td>
                </tr>
            <?php } ?>
            <tr valign="top">
                <th scope="row"></th>
                <td>
                    <input type="submit" name="saveData" value="بروزرسانی" style="cursor: pointer;">
                    <button type="submit"><a style="text-decoration: none; color: #000;" href="http://wordpress.exp/wp-admin/admin.php?page=apis_admin">صفحه اصلی</a></button>
                </td>
            </tr>
        </table>
    </form>
</div>