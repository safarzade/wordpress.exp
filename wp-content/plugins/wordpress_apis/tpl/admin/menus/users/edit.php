<div class="wrap">
    <h1>ویرایش اطلاعات کاربر</h1>
    <form method="POST" action="">
        <table class="form-table">
            <tr valign="top">
                <th scope="row">تلفن همراه</th>
                <td>
                    <input type="text" name="mobile" value="<?php echo $mobile; ?>">
                </td>
            </tr>
            <tr valign="top">
                <th scope="row">کیف پول</th>
                <td>
                    <input type="text" name="wallet" value="<?php echo $wallet; ?>">
                </td>
            </tr>
            <tr valign="top">
                <th scope="row"></th>
                <td>
                    <button name="saveUserInfo" class="button" type="submit" style="cursor: pointer;">ذخیره سازی اطلاعات</button>
                </td>
            </tr>
        </table>
    </form>
</div>