<div class="wrap">
    <h1>کاربران ویژه</h1>
    <table class="widefat">
        <thead>
            <tr>
                <td>آیدی</td>
                <td>نام</td>
                <td>ایمیل</td>
                <td>تلفن همراه</td>
                <td>کیف پول</td>
                <td>عملیات</td>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($users as $user) : ?>
                <?php
                $userWallet = get_user_meta($user->ID, 'wallet', true);
                $userWallet = empty($userWallet) ? 0 : $userWallet;
                ?>
                <tr>
                    <td><?php echo $user->ID; ?></td>
                    <td><?php echo $user->display_name; ?></td>
                    <td><?php echo $user->user_email; ?></td>
                    <td>
                        <a style="color: #aaa;" href="<?php echo add_query_arg(["action" => "removeMobile", 'id' => $user->ID]); ?>">
                            <span class="dashicons dashicons-trash"></span>
                        </a>
                        <?php echo get_user_meta($user->ID, 'mobile', true); ?>
                    </td>
                    <td>
                        <a style="color: #aaa;" href="<?php echo add_query_arg(["action" => "removeWallet", 'id' => $user->ID]); ?>">
                            <span class="dashicons dashicons-trash"></span>
                        </a>
                        <?php echo number_format($userWallet) . " تومان"; ?>
                    </td>
                    <td>
                        <a style="color: #aaa; margin-left: 10px;" href="<?php echo add_query_arg(["action" => "edit", 'id' => $user->ID]); ?>">
                            <span class="dashicons dashicons-edit"></span>بروزرسانی
                        </a>
                        <a style="color: #aaa;" href="<?php echo add_query_arg(["action" => "removeMobileAndWallet", 'id' => $user->ID]); ?>">
                            <span class="dashicons dashicons-trash"></span>حذف
                        </a>
                    </td>
                </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
</div>