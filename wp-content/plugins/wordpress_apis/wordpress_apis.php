<?php
/*
Plugin Name: wordpress_apis
Plugin URI: https://google.com/
Description: wordpress_apis
Author: Mostafa Akrami
Author URI: https://google.com/
Text Domain: wordpress_apis 
Domain Path: /languages/
Version: 1.0.0
*/

define("DIR" , plugin_dir_path(__FILE__));
define("URL" , plugin_dir_url(__FILE__));
define("INC", DIR . "/inc");
define("TPL" , DIR . "/tpl");
register_activation_hook(__FILE__ , "apis_active");
register_deactivation_hook(__FILE__ , "apis_deactive");

function apis_active(){
    add_role(
        "shop-manager" ,
        "shop manager",
        [
            'read' => true,
            'edit_posts' => true,
            'remove_products' => true
        ]
    );
    $role = get_role('administrator');
    $role->add_cap('remove_products');
}

function apis_deactive(){

}

if(is_admin()){
    include INC . "/admin/menus.php";
    include INC . "/admin/metaboxes.php";
}
include INC . "/ajax.php";

function wpapis_register_style(){
    wp_register_style("wpapis_main_style" , URL . "assets/css/main.css");
    wp_enqueue_style("wpapis_main_style");

    if(is_admin()){
        wp_register_script("wpapis_main_js", URL . "assets/js/script-admin.js");
        wp_enqueue_script("wpapis_main_js");
    }
    else{
        wp_register_script("wpapis_main_user_js", URL . "assets/js/script-user.js" , ['jquery'] , '1.5.0' , true);
        wp_enqueue_script("wpapis_main_user_js");
    }
}

add_action("wp_enqueue_scripts" , "wpapis_register_style");
add_action("admin_enqueue_scripts" , "wpapis_register_style");


