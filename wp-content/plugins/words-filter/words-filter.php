<?php
/*
Plugin Name: فیلترسازی کلمات
Plugin URI: https://google.com/
Description: این یک افزونه ساده برای فیلترسازی کلمات می باشد .
Author: Mostafa Akrami
Author URI: https://google.com/
Text Domain: wordsfilter
Domain Path: /languages/
Version: 1.0.0
*/

define("WF_DIR", plugin_dir_path(__FILE__));
define("WF_URL", plugin_dir_url(__FILE__));
define("WF_INC", WF_DIR . "/inc");


function wf_filter_words($content){
    $word = 'وردپرس';
    $replace = '<a target="_blank" href="https://wordpress.net">وردپرس</a>';
    $strlength = mb_strlen($word);
    // $content = preg_replace("/{$word}/" , $replace , $content);
    // $content = preg_replace("/{$word}/", str_repeat('*' , $strlength) , $content);
    $content = preg_replace("/{$word}/" , '' , $content);
    return $content;
}

add_filter("the_content" , "wf_filter_words");