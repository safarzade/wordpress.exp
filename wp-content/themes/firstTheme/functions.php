<?php 

define("EXP_PATH" , get_template_directory());
define("EXP_URL" , get_template_directory_uri());

add_action("wp_enqueue_scripts" , "firstTheme_add_assets");

function firstTheme_add_assets(){
    wp_register_style("theme_main_style" , EXP_URL . "/assets/css/main.css");
    wp_enqueue_style("theme_main_style");

    
    if(is_single() ){
        wp_register_script("theme_main_script", EXP_URL . "/assets/js/main.js", ['jquery'], false, true);
        wp_enqueue_script("theme_main_script");
    }
    
}

add_action("after_setup_theme" , "theme_setup");

function theme_setup(){
    add_theme_support("post-thumbnails");
    add_theme_support("title-tag");
    add_theme_support( 'post-formats', array( 'aside', 'gallery' , 'audio' , 'video' , 'chat' , 'link' , 'image'));

    add_filter("show_admin_bar" , "__return_false");

}

function wpdocs_theme_slug_widgets_init() {
    register_sidebar( array(
        'name'          => __( 'Main Sidebar', 'textdomain' ),
        'id'            => 'sidebar-1',
        'description'   => __( 'Widgets in this area will be shown on all posts and pages.', 'textdomain' ),
        'before_widget' => '<li id="%1$s" class="widget %2$s">',
        'after_widget'  => '</li>',
        'before_title'  => '<h2 class="widgettitle">',
        'after_title'   => '</h2>',
    ) );
}
add_action( 'widgets_init', 'wpdocs_theme_slug_widgets_init' );

include EXP_PATH . "/includes/frontend/functions.php";
include EXP_PATH . "/includes/frontend/post-type.php";
include EXP_PATH . "/includes/frontend/taxonomies.php";
include EXP_PATH . "/includes/frontend/menus.php";
include EXP_PATH . "/includes/widgets.php";





