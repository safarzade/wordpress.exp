<?php 

function register_auther_taxonomoy(){

    $args = array(
        'labels'            => __("Auther" , "textdomain"),
        'public'            => true,
        'rewrite'           => true,
        'hierarchical'      => true
    );

    register_taxonomy( 'auther', 'book', $args );
}

add_action("init" , "register_auther_taxonomoy");